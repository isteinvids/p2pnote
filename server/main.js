
var io = require("socket.io")();
var crypto = require('crypto');

function md5(str){
	return crypto.createHash('md5').update(str).digest('hex');
}

var allsockets = {};
var publickeys = {};

io.on("connection", function(socket){

	allsockets[socket.id] = socket;

	socket.on("publickey", function(data){
		publickeys[socket.id] = data;
	});
	socket.on("request_note_from_server", function(data){
		//check data, make sure it's a valid md5 hash
		io.emit("request_note_from_client", { "md5" : data["md5"], "to" : socket.id, "publickey" : publickeys[socket.id] });
		//																				^^^ client has to trust that the server has the correct public key,
		// 																					and isn't just faking it... might be a security problem
	});
	socket.on("send_note_from_client", function(data){
		//TODO : make the two clients encrypt everything with RSA so server can't read the note!
		//but then it would be up to the client to verify the note with the md5, but that isn't a big deal

		if(allsockets[data['to']] && allsockets[data['to']].connected && publickeys[socket.id]){
			if(typeof(data['to']) === 'string' && socket.id !== data['to'] && allsockets[data['to']] && allsockets[data['to']].connected){
				allsockets[data['to']].emit("send_note_from_server", data);
			}
		}
	});
	socket.on("disconnect", function(data){
		delete(allsockets[socket.id]);
	});
});

io.listen(8080);