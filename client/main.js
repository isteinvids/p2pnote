var NodeRSA = require("node-rsa");
var io = require('socket.io-client');

var key = new NodeRSA({b: 512});

var publicKey = key.exportKey('pkcs8-public');

var note_received = false;

var note_request_interval_id = -1;

var note_md5 = location.search.substr(1); // if not null, then we don't have note and we need to request it from someone else

if(note_md5){
	document.getElementById("create_new").hidden = true;
} else {
	document.getElementById("create_new").hidden = false;
}

function log(str){
	document.getElementById("console").innerText+=(str+"\n")
}
// var note_str = "klfmlsdjlfnsdkfjnsdf";

function showNote(note){
	document.getElementById("note").innerText = note;
}

function hasNote(md5){
	return typeof localStorage.getItem(md5) === 'string';
}
function getNote(md5){
	return localStorage.getItem(md5);
}
function setNote(md5, note){
	localStorage.setItem(md5, note);
}

document.getElementById("theform").onsubmit = function(){
	var themd5 = md5(document.getElementById("thetextarea").value);
	setNote(themd5, document.getElementById("thetextarea").value);
	location.search = themd5;
	return false;
}

var socket = io.connect('http://localhost:8080');

socket.on('connect', function () {
	log("Connected to websocket");

	socket.emit("publickey", publicKey);
	log("Sending public key to server");
	if (note_md5) {
		if(!hasNote(note_md5) || note_md5 !== md5(getNote(note_md5))){

			log("Requesting note with md5 " + note_md5);
			socket.emit("request_note_from_server", { "md5": note_md5 });

			note_request_interval_id = setInterval(function(){
				if(note_received){
					return;
				}
				log("Requesting note with md5 " + note_md5);
				socket.emit("request_note_from_server", { "md5": note_md5 });
			}, 30000);
		} else {
			log("Already have valid note stored, showing that instead of requesting from server");
			showNote(getNote(note_md5));
		}
	}
});
socket.on('request_note_from_client', function (obj) {
	if(obj['to'] !== socket.id){
		log("Server is requesting note with md5 " + obj['md5']);
		if(hasNote(obj['md5'])){
			log("Have note stored for " + obj['md5'] + ", sending that to the person encrypted with their publickey");

			var theirPublicKey = new NodeRSA(obj['publickey'], 'pkcs8-public');
			obj['note'] = theirPublicKey.encrypt(getNote(obj['md5']), 'base64');
			socket.emit("send_note_from_client", obj);
		} else {
			log("Don't have the note " + obj['md5'] + " stored :(");
		}
	}
});
socket.on('send_note_from_server', function(data){
	data['note'] = key.decrypt(data['note'], 'utf8'); // decrypt note with our private key
	if(data['md5'] === md5(data['note']) && note_md5 === data['md5']){
		log("md5 matched");
		setNote(data['md5'], data['note']);
		showNote(data['note']);
		clearInterval(note_request_interval_id);
		note_received = true;
	} else {
		log("md5 not matched?!!?!?!?");
	}
});
socket.on('disconnect', function () {
	log("Disconnected from websocket");
});